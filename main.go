package main

import (
	"encoding/json"
	"log"
	"os"
	"time"

	"bitbucket.org/nayar/mqclient"
	"github.com/jaracil/ei"
	"github.com/jessevdk/go-flags"
	"github.com/nayarsystems/nxsugar-go"
)

var opts struct {
	Config string `short:"c" default:"config.json" description:"config file"`
	Production bool `long:"production" description:"Log as json"`
}

type MqSpaceConfig struct {
	Url string `json:url`
	Prefix string `json:prefix`
}

var mqSpaceConfig *MqSpaceConfig

func main () {
	_, err := flags.Parse(&opts)
	if err != nil {
		log.Fatalln(err)
		os.Exit(1)
	}

	mqSpaceConfig, _ = readMqSpaceConfig(opts.Config)

	nxsugar.SetFlagsEnabled(false)
	nxsugar.SetConfigFile(opts.Config)
	nxsugar.SetProductionMode(opts.Production)
	srv, err := nxsugar.NewServiceFromConfig("mqspace-auth")
	if err != nil {
		log.Fatalln(err)
	}
	srv.AddMethod("login", loginHandler)

	err = srv.Serve()
	if err != nil {
		log.Println("Lost connection with nexus:", err)
	}
}

func loginHandler(task *nxsugar.Task) (interface{}, *nxsugar.JsonRpcErr) {
	username := ei.N(task.Params).M("user").StringZ()
	password := ei.N(task.Params).M("pass").StringZ()

	mqSession := mqclient.NewSession(mqSpaceConfig.Url)
	if err := mqSession.Login(mqSpaceConfig.Prefix + username, password); err != nil {
		return nil, &nxsugar.JsonRpcErr{Cod: 1, Mess: "Invalid user or password"}
	}
	if err := mqSession.Start(); err != nil {
		return nil, &nxsugar.JsonRpcErr{Cod: 2, Mess: "Session error: " + err.Error()}
	}

	nexusUsername, err := getNexusUser(mqSession)
	if err != nil {
		return nil, &nxsugar.JsonRpcErr{Cod: 3, Mess: "Error obtaining Nexus user: " + err.Error()}
	}

	mqSession.Stop()
	mqSession.Logout()

	return map[string]interface{}{"user": nexusUsername}, nil
}

func getNexusUser(mqSession *mqclient.Session) (string, error) {
	message := map[string]interface{}{
		"type": "task_push",
		"data": map[string]interface{}{
			"path": "/root/manager/api",
			"data": map[string]interface{}{
				"func": "whoami",
				"args": map[string]interface{}{},
			},
		},
	}

	command := mqSession.NewCommand()
	command.Send(message)
	res, err := command.Wait(60 * time.Second)

	if err != nil {
		return "", err
	}

	return ei.N(res["data"]).M("result").M("nexusUser").StringZ(), nil
}

func readMqSpaceConfig(configFile string) (*MqSpaceConfig, error) {
	file, _ := os.Open(configFile)
	defer file.Close()

	config := map[string]interface{}{}
	decoder := json.NewDecoder(file)
	if err := decoder.Decode(&config); err != nil {
		return nil, err
	}

	mqConfig := &MqSpaceConfig{
		ei.N(config["mqspace"]).M("url").StringZ(),
		ei.N(config["mqspace"]).M("prefix").StringZ(),
	}
	return mqConfig, nil
}
